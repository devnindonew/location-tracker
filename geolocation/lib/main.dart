import 'package:flutter/material.dart';
import 'package:geolocation/dataModels/userLocation.dart';
import 'package:geolocation/services/location_services.dart';
import 'package:geolocation/views/locationView.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserLocation>(
      create: (context) => LocationService().locationStream,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'GeoLocation',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          body: LocationView(),
        ),
      ),
    );
  }
}
