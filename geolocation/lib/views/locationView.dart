import 'package:flutter/material.dart';
import 'package:geolocation/dataModels/userLocation.dart';
import 'package:provider/provider.dart';

class LocationView extends StatelessWidget {
  const LocationView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var userLocation = Provider.of<UserLocation>(context);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Current Location"),
          Text("Latitude:${userLocation.latitude}"),
          Text("Longitude:${userLocation.longitude}"),
          Text("Altitude:${userLocation.altitude}"),
        ],
      )
    );
  }
}
