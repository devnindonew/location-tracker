import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentAddress = " ";
  double distanceInMeters = 0;

  @override
  void initState() {
    _locationStream();
    super.initState();
  }

  _locationStream() {
    geolocator.checkGeolocationPermissionStatus().then((granted) async {
      if (granted != null) {
        geolocator
            .getPositionStream(LocationOptions(
                accuracy: LocationAccuracy.best, timeInterval: 1000))
            .listen((position) {
          if (position != null) {
            _currentPosition = position;
            _matchLocation();
          }
        });
      }
    });
  }

  _matchLocation() async {
    distanceInMeters = await Geolocator().distanceBetween(23.7510177, 90.378978,
        _currentPosition.latitude, _currentPosition.longitude);

    if (distanceInMeters < 10) {
      _getAddressFromLatLng();
    }
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple[800],
        title: Text("geoLocation"),
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.grey[50],
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: (_currentPosition != null)
                    ? Column(
                        children: <Widget>[
                          Text(
                            "Current Geo-Location",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "Latitude: ${_currentPosition.latitude}",
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w300),
                          ),
                          Text(
                            "Longitude: ${_currentPosition.longitude}",
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w300),
                          ),
                        ],
                      )
                    : Text(
                        "Please, enable location",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w400),
                      ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.grey[50],
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: (0 < distanceInMeters && distanceInMeters < 10)
                    ? Column(
                        children: <Widget>[
                          Text(
                            "Location Matched",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "Your Location",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                          Text(
                            "$_currentAddress",
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w300),
                          ),
                        ],
                      )
                    : Column(
                        children: <Widget>[
                          Text(
                            "Target Location",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "Latitude:23.7510177 & Longitude: 90.378978",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          )
                        ],
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
