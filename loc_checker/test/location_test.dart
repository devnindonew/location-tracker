import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:loc_checker/service/locationService.dart';
import 'package:test/test.dart';

main() {
  WidgetsFlutterBinding.ensureInitialized();
  test('type tst', () {
    Stream<Position> position = LocationService().getLocation;
    
    expect(position, Position);
    
  });
}
