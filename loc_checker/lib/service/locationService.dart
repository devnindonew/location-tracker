import 'dart:async';
import 'package:geolocator/geolocator.dart';

class LocationService {
  Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position currentLocation;
  String currentAddress;
  double definedLatitude = 23.7510177;
  double definedLongitude = 90.378978;
  var timer = Duration(seconds: 10);

  StreamController<Position> _locationController = StreamController<Position>();
  StreamController<String> _distanceController = StreamController<String>();

  LocationService() {
    matchLocation();
    Timer.periodic(timer, (t) {
      matchLocation();
    });
  }

  Stream<Position> get getLocation => _locationController.stream;
  Stream<String> get getAddress => _distanceController.stream;

  Future<Position> getCurrentLocation() async {
    Position position = await geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  Future<double> findDistance() async {
    Position position = await getCurrentLocation();
    double distanceInMeters = await Geolocator().distanceBetween(definedLatitude,
        definedLongitude, position.latitude, position.longitude);
    return distanceInMeters;
  }

  matchLocation() async {
    currentLocation = await getCurrentLocation();
    
    double distance = await findDistance();
    if (distance < 10) {
      try {
        List<Placemark> p = await geolocator.placemarkFromCoordinates(
            currentLocation.latitude, currentLocation.longitude);

        Placemark place = p[0];
        currentAddress =
            "${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}";
      } catch (e) {
        print(e);
      }
    } else {
      currentAddress = null;
    }

    _locationController.add(currentLocation);
    _distanceController.add(currentAddress);
  }
}
