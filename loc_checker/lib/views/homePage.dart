import 'package:flutter/material.dart';
import 'package:loc_checker/customWidgets/currentAddress.dart';
import 'package:loc_checker/customWidgets/currentLocation.dart';
import 'package:loc_checker/service/locationService.dart';

import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<String>(
      create: (context) => LocationService().getAddress,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple[800],
          title: Text("geoLocation"),
        ),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.all(
                Radius.circular(25),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CurrentLocation(),
                SizedBox(
                  height: 20,
                ),
                CurrentAddress(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
