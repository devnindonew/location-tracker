import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CurrentAddress extends StatefulWidget {
  @override
  _CurrentAddressState createState() => _CurrentAddressState();
}

class _CurrentAddressState extends State<CurrentAddress> {
  @override
  Widget build(BuildContext context) {
    var currentAddress = Provider.of<String>(context);
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.grey[50],
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: (currentAddress != null)
          ? Column(
              children: <Widget>[
                Text("Location Matched",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),),
                Text(
                  "Now you are at",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                Text(
                  "$currentAddress",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
              ],
            )
          : Column(
              children: <Widget>[
                Text(
                  "You are out of range",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                Text("Target location", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                Text(
                  "latitude:23.7510177, longitude: 90.378978",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
                )
              ],
            ),
    );
  }
}
