import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:loc_checker/service/locationService.dart';
import 'package:loc_checker/views/homePage.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<Position>(
      create: (context) => LocationService().getLocation,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'geoLocation',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}
